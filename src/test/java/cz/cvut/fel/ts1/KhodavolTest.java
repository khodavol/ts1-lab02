package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class KhodavolTest {

    @Test
    public void factorial_nIs5_returns120(){
        //arrange
        Khodavol khodavol = new Khodavol();

        //act
        int actual = khodavol.factorial(5);

        //assert
        Assertions.assertEquals(120,actual);
    }

    @Test
    public void factorial_nIs0_returns1(){
        //arrange
        Khodavol khodavol = new Khodavol();

        //act
        int actual = khodavol.factorial(0);

        //assert
        Assertions.assertEquals(1,actual);
    }

    @Test
    public void factorial_nIsMinus1_returns0(){
        //arrange
        Khodavol khodavol = new Khodavol();

        //act
        int actual = khodavol.factorial(-1);

        //assert
        Assertions.assertEquals(0,actual);
    }
}
