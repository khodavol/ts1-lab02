package cz.cvut.fel.ts1;

public class Khodavol {

    public int  factorial (int n){
        if(n<0) return 0;

        int result  = n;
        for (int i = 1; i < n; i++){
            result = result * (n+1);
        }
        return result;
    }
}
